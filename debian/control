Source: harminv
Section: science
Priority: optional
Maintainer: Thorsten Alteholz <debian@alteholz.de>
Build-Conflicts: libatlas-base-dev
Build-Depends: debhelper-compat (= 13),
	liblapack-dev,
	gfortran
Standards-Version: 4.6.2
Homepage: https://github.com/stevengj/harminv/
Vcs-Browser: https://salsa.debian.org/alteholz/harminv
Vcs-Git: https://salsa.debian.org/alteholz/harminv.git
Rules-Requires-Root: no

Package: harminv
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}
Description: extraction of complex frequencies and amplitudes from time series
 Harminv is a free program to solve the problem of harmonic inversion, given
 a discrete-time, finite-length signal that consists of a sum of finitely-many
 sinusoids (possibly exponentially decaying) in a given bandwidth, it
 determines the frequencies, decay constants, amplitudes, and phases of those
 sinusoids.

Package: libharminv3
Section: libs
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}
Description: Library for using harminv
 Libharminv is a free library to solve the problem of harmonic inversion,
 given a discrete-time, finite-length signal that consists of a sum of
 finitely-many sinusoids (possibly exponentially decaying) in a given
 bandwidth, it determines the frequencies, decay constants, amplitudes, and
 phases of those sinusoids.
 .
 This package contains the library.

Package: libharminv-dev
Section: libdevel
Architecture: any
Depends: libharminv3 (= ${binary:Version}), ${misc:Depends}
Description: Library for using harminv, development version
 Libharminv is a free library to solve the problem of harmonic inversion,
 given a discrete-time, finite-length signal that consists of a sum of
 finitely-many sinusoids (possibly exponentially decaying) in a given
 bandwidth, it determines the frequencies, decay constants, amplitudes, and
 phases of those sinusoids.
 .
 This package contains the header files.
